#!/usr/bin/env bash

pushd $(dirname ${BASH_SOURCE[0]})
export SERVICE=$(basename $PWD)
export SERVICE_NAME=$(egrep -o "[^_]+$" <<<"$SERVICE_NAME")
export SERVICE_VERSION=$(git describe --tags)

mkdir .wheels
cp -r $WHEELHOUSE/* .wheels/

echo "docker build --build-arg SERVICE_NAME=$SERVICE_NAME --build-arg SERVICE_VERSION=$SERVICE_VERSION -t listenup/$SERVICE:$SERVICE_VERSION ."

docker build --build-arg SERVICE_NAME=$SERVICE_NAME --build-arg SERVICE_VERSION=$SERVICE_VERSION -t listenup/$SERVICE:$SERVICE_VERSION .

rm -rf .wheels
popd
