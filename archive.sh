#!/bin/bash

TMP_FOLDER='/tmp/loan_app'

rm loan_app.tgz

mkdir ${TMP_FOLDER}

rsync -ax --exclude .git --exclude .env . ${TMP_FOLDER}

# ls -alh ${TMP_FOLDER}

tar --exclude=.git --exclude=*.pyc --exclude=__pycache__ --exclude=.idea --exclude=loan_app.tgz -zcf loan_app.tgz -C "${TMP_FOLDER}/" .

rm -rf ${TMP_FOLDER}
