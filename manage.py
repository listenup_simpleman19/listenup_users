import os
import subprocess
import contextlib

from flask import current_app as app
from flask_migrate import MigrateCommand
from flask_script import Manager

from users import create_app, db
from users.models import User

manager = Manager(create_app)

manager.add_command('db', MigrateCommand)

root_path = os.path.dirname(os.path.abspath(__file__))


def _make_context():
    return dict(app=manager.app, db=db)


@manager.option('-d', '--drop_first', help='Drop tables first?')
def createdb(drop_first=True):
    """Creates the database."""
    db.session.commit()
    if drop_first:
        print("Dropping all databases")
        db.drop_all()
    db.create_all()


@manager.command
def test():
    """Run unit tests"""
    tests = subprocess.Popen(['python', '-m', 'unittest'])
    tests.wait()
    with contextlib.suppress(FileNotFoundError):
        os.remove('files/test.db')


@manager.command
def resettobase():
    db.session.commit()
    db.drop_all()
    db.create_all()


if __name__ == '__main__':
    manager.run()
