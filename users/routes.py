import os
from flask import Blueprint, render_template, session, redirect, url_for, request, current_app as app, g

from listenup_common.api_response import ApiException, ApiMessage, ApiResult
from listenup_common.logging import get_logger
from users.auth import multi_auth, create_token
from users.models import User
from . import db
import datetime

main = Blueprint('main', __name__, url_prefix='/api/users')

root_path = os.path.dirname(os.path.abspath(__file__))

logger = get_logger(__name__)


@main.route('/login', methods=['POST'])
@multi_auth.login_required
def login_user():
    logger.info("Logging in user: %s", g.current_user_guid)
    user = User.query.filter_by(guid=g.current_user_guid).one_or_none()
    return ApiResult(value={
        'token': create_token(g.current_user_guid).decode('utf-8'),
        'user': user.to_dict(),
    }).to_response()


@main.route("/signup", methods=['POST'])
def signup():
    if request.method == 'POST' and request.form:
        # Hiding passwords (please never log a password...)
        printed_form = request.form.copy()
        printed_form['password'] = "Hidden"
        printed_form['confirm'] = "Hidden"
        logger.info("Signing up new user: %s", printed_form)
        if all(k in request.form for k in ("username", "display_name", "password", "email", "confirm", "accept_tos")) and request.form["accept_tos"]:
            # Check for user
            if User.query.filter_by(username=request.form["username"]).one_or_none():
                logger.info("User already exists for username: %s, not adding a new user", request.form["username"])
                raise ApiException(message="Username already exists",
                                   status=409,
                                   errors=["Username already exists, please try again"])
            elif User.query.filter_by(email=request.form["email"]).one_or_none():
                logger.info("User already exists for username: %s, not adding a new user", request.form["email"])
                raise ApiException(message="Username already exists",
                                   status=409,
                                   errors=["Email already exists, reset your password?"])
            user = User(username=request.form["username"],
                        display_name=request.form["display_name"],
                        password=request.form["password"],
                        email=request.form["email"])
            db.session.add(user)
            db.session.commit()
            # Go ahead and sign new user in
            g.current_user_guid = user.guid
            response_dict = user.to_dict()
            response_dict['token'] = create_token(user_id=user.guid).decode('utf-8')
            logger.info("Successfully added a new user: %s", user.to_dict())
            resp = ApiResult(value=response_dict, status=200).to_response()
        else:
            logger.error("Failed to sign up new user for: %s", request.form)
            raise ApiException(message="Form is missing fields for user signup", status=400)
        return resp
    else:
        logger.warn("Signup request did not have a form in the request")
        raise ApiException(message="Invalid request or form", status=409)


@main.route("/me", methods=['GET', 'POST'])
@multi_auth.login_required
def me():
    logger.info("Getting information about currently logged in user: %s", g.current_user_guid)
    user = User.query.filter_by(guid=g.current_user_guid).one_or_none()
    if user:
        response_dict = {'user': user.to_dict()}
        logger.info("Creating new token for user: %s", g.current_user_guid)
        response_dict['token'] = create_token(user_id=user.guid).decode('utf-8')
        return ApiResult(value=response_dict, status=200).to_response()
    else:
        raise ApiException(message="Unauthorized user", status=401)


@main.route("/logout")
def logout():
    logger.info("Logging out user by clearing cookies and expiring them now")
    resp = redirect(url_for('main.home'))
    resp.set_cookie('Bearer', '', expires=datetime.datetime.now())
    session['login_failed'] = False
    return resp


@main.route("/user/<string:guid>", methods=["GET"])
@main.route("/<string:guid>", methods=["GET"])
def get_user(guid):
    logger.info("Getting user information for guid: %s", guid)
    user = User.query.filter_by(guid=guid).one_or_none()
    if user:
        logger.debug("Returning user information: %s", user.to_dict())
        return ApiResult(value=user.to_dict(), status=200).to_response()
    else:
        logger.warn("Could not find user for guid: %s", guid)
        raise ApiException(message="Could not find user for gud: " + guid, status=404)


@main.errorhandler(ApiException)
def handle_api_exception(error: ApiException):
    return error.to_result().to_response()
