from flask import current_app as app
from datetime import datetime
from listenup_common.utils import guid, app_key
from flask_sqlalchemy import SQLAlchemy
import bcrypt

db = SQLAlchemy()


class BaseModel(db.Model):
    __abstract__ = True
    id = db.Column(db.Integer, primary_key=True)
    creation_timestamp = db.Column(db.DateTime, nullable=False,
                                   default=datetime.utcnow)


class User(BaseModel):
    __tablename__ = 'user'
    email = db.Column(db.String(250), nullable=False)
    username = db.Column(db.String(40), nullable=False, unique=True)
    display_name = db.Column(db.String(250), nullable=False)
    password = db.Column(db.LargeBinary(100), nullable=False)
    user_level = db.Column(db.Integer, nullable=False, default=0)
    admin = db.Column(db.Boolean, default=False, nullable=False)
    guid = db.Column(db.String(32), nullable=False)

    def verify_password(self, password):
        if password:
            return bcrypt.checkpw(password.encode('utf-8'), self.password)
        else:
            return False

    def set_password(self, new_password, old_password=""):
        if self.password is None or self.password == "":
            self.password = bcrypt.hashpw(new_password.encode('utf-8'), bcrypt.gensalt())
            return True
        elif bcrypt.checkpw(old_password.encode('utf-8'), self.password):
            self.password = bcrypt.hashpw(new_password.encode('utf-8'), bcrypt.gensalt())
            return False

    def to_dict(self):
        return {
            'id': self.id,
            'email': self.email,
            'creation_timestamp': self.creation_timestamp,
            'username': self.username,
            'display_name': self.display_name,
            'user_level': self.user_level,
            'admin': self.admin,
            'guid': self.guid
        }

    def __init__(self, username="", password="", email="", display_name=""):
        self.username = username
        if not password == "":
            self.set_password(password)
        self.email = email
        self.display_name = display_name
        self.guid = guid()

    def __str__(self):
        return 'Username: ' + self.username + ' id: ' + str(self.id)

    def __repr__(self):
        return 'User()'

    def __eq__(self, other):
        """Override the default Equals behavior"""
        if isinstance(other, self.__class__):
            return self.id == other.id
        return NotImplemented

    def __ne__(self, other):
        """Define a non-equality test"""
        if isinstance(other, self.__class__):
            return not self.__eq__(other)
        return NotImplemented


class Application(BaseModel):
    __tablename__ = 'application'
    guid = db.Column(db.String(32), nullable=False)
    application_name = db.Column(db.String(60), nullable=False, unique=False)
    application_key = db.Column(db.String(64), nullable=False, unique=True)
    expired = db.Column(db.Boolean, nullable=False, default=False)
    key_creator_guid = db.Column(db.String(32), nullable=False, unique=False)
    expiration = db.Column(db.BigInteger, nullable=False, default=0)
    renewable = db.Column(db.Boolean, nullable=False, default=True)

    def __init__(self, application_name, key_creator_guid):
        self.guid = guid()
        self.application_key = app_key()
        self.application_name = application_name
        self.key_creator_guid = key_creator_guid

    def to_dict(self):
        return {
            'id': self.id,
            'guid': self.guid,
            'application_name': self.application_name,
            'application_key': self.application_key,
            'expired': self.expired,
            'expiration': self.expiration,
            'renewable': self.renewable,
            'created': self.creation_timestamp,
        }


class KeyCreator(BaseModel):
    __tablename__ = 'key_creator'
    guid = db.Column(db.String(32), nullable=False)
    application_key = db.Column(db.String(32), nullable=False, unique=True)
    expired = db.Column(db.Boolean, nullable=False, default=False)
    expiration = db.Column(db.BigInteger, nullable=False, default=0)
    renewable = db.Column(db.Boolean, nullable=False, default=True)

    def __init__(self):
        self.guid = guid()
        self.application_key = app_key()
