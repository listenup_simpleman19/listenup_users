from listenup_common.http_auth import HTTPBasicAuth, MultiAuth, HTTPTokenAuth
from listenup_common.api_response import ApiException, ApiResult
from listenup_common.logging import get_logger
from flask import request, redirect, url_for, jsonify, Blueprint, current_app as app, g
from users.models import User, Application, db
from datetime import timedelta, datetime
import jwt

basic_auth = HTTPBasicAuth()
token_auth = HTTPTokenAuth('Bearer')
token_optional_auth = HTTPTokenAuth('Bearer')
multi_auth = MultiAuth(basic_auth, token_auth)

auth_blueprint = Blueprint('auth', __name__, url_prefix='/api/users')

logger = get_logger(__name__)


@basic_auth.verify_password
def verify_password(username, password):
    if not username or not password:
        logger.info("Did not find username or password from basic auth header so checking form for authentication")
        if "username" in request.form and request.form['username']:
            logger.debug("Found username: %s in form so attempting to authenticate", request.form['username'])
            user = User.query.filter_by(username=request.form['username']).one_or_none()
            if user and user.verify_password(request.form.get('password')):
                logger.info("Successfully logged in username: %s as user: %s", username, user.guid)
                g.current_user_guid = user.guid
                return True
            if not user:
                logger.warn("Could not find user for username: %s", request.form["username"])
            elif not request.form.get('password'):
                logger.debug("Could not find password in form for username: %s user: %s", request.form["username"], user.guid)
            else:
                logger.info("Invalid password in form for username: %s user: %s", request.form["username"], user.guid)
        else:
            # Hiding password (please never log a password...)
            printed_form = request.form.copy()
            if "password" in printed_form:
                printed_form['password'] = "Hidden"
            logger.debug("Could not find username in form: %s", printed_form)
        return False
    logger.info("Verifying password for username: %s", username)
    user = User.query.filter_by(username=username).one_or_none()
    if user is None or not user.verify_password(password):
        logger.info("Invalid password for username: %s", username)
        return False
    g.current_user_guid = user.guid
    logger.info("Authenticated username: %s guid: %s using basic auth", username, user.guid)
    return True


@basic_auth.error_handler
def password_error():
    """Return a 401 error to the client."""
    resp = ApiException(message="Invalid Login", status=401).to_response()
    resp.headers.add_header('WWW-Authenticate', 'Bearer realm="Authentication Required"')
    return resp


@token_auth.verify_token
def verify_token(token):
    logger.info("Verifying token")
    decoded = None
    try:
        decoded = decode_token(token, catch_exceptions=False)
        logger.info("Token successfully decoded for user: %s", decoded.get("user_guid", None))
    except jwt.ExpiredSignatureError:
        decoded = decode_token(token, catch_exceptions=True)
        logger.warn('Expired Token for user: %s', decoded.get("user_guid", None))
        return False
    except jwt.DecodeError:
        logger.error('Decode Token Error')
        return False
    g.token = decoded
    g.current_user_guid = decoded['user_guid']
    return True


@token_auth.error_handler
def token_error():
    """Return a 401 error to the client."""
    return ApiException(message="Authentication Required", status=401).to_response()


@token_optional_auth.verify_token
def verify_optional_token(token):
    if not token:
        g.token = None
        g.current_user_guid = None
        return True
    return verify_token(token)


@token_optional_auth.error_handler
def multi_error():
    """Return a 401 error to the client."""
    return ApiException(message="Authentication Required", status=401).to_response()


def create_token(user_id=None, resp=None, **kwargs):
    if user_id is not None:
        if get_current_token():
            token_dict = get_current_token()
            for key, value in kwargs.items():
                token_dict[key] = value
            token_dict['exp'] = (datetime.utcnow() + timedelta(hours=app.config['TOKEN_EXPIRE_MINUTES']))
            token = jwt.encode(token_dict, app.config['JWT_SECRET_KEY'], algorithm='HS256')
        else:
            token = jwt.encode({'user_guid': str(user_id),
                               'exp': (datetime.utcnow() + timedelta(hours=app.config['TOKEN_EXPIRE_MINUTES']))
                               }, app.config['JWT_SECRET_KEY'], algorithm='HS256')
        if resp:
            resp.set_cookie('Bearer', token, expires=datetime.now() +
                            timedelta(minutes=app.config['TOKEN_EXPIRE_MINUTES']))
        return token
    else:
        return ""


def get_current_token():
    if request.cookies.get('Bearer'):
        token = decode_token(request.cookies.get('Bearer'), catch_exceptions=True)
    else:
        token = None
    return token


def decode_token(token, catch_exceptions=True, verify=True):
    if catch_exceptions:
        try:
            decoded = jwt.decode(token, app.config['JWT_SECRET_KEY'], algorithms=['HS256'], verify=verify)
        except jwt.ExpiredSignatureError:
            logger.warn('Decoded an expired Token')
            return None
        except jwt.DecodeError:
            logger.warn('Tried to decode an invalid token')
            return None
    else:
        decoded = jwt.decode(token, app.config['JWT_SECRET_KEY'], algorithms=['HS256'], verify=verify)
    return decoded


@auth_blueprint.route('/token', methods=['GET', 'POST'])
@auth_blueprint.route('/refresh', methods=['GET', 'POST'])
@multi_auth.login_required
def get_token():
    logger.info("Refreshing token for user: %s", g.current_user_guid)
    if g.current_user_guid is None:
        logger.warn("Token request for user that is not logged in")
        return password_error()
    logger.debug("Getting token for: %s", g.current_user_guid)
    token = create_token(g.current_user_guid)
    resp = jsonify({'token': token.decode('UTF-8')})
    resp.set_cookie('Bearer',
                    token,
                    expires=datetime.now() + timedelta(minutes=app.config['TOKEN_EXPIRE_MINUTES']))
    logger.debug("Successfully refreshed token for user: %s", g.current_user_guid)
    return resp


@auth_blueprint.route("/verify", methods=['POST'])
def verify_token():
    logger.info("Attempting to verify token")
    if request.json and request.json.get("token"):
        logger.debug("Found token in json")
        try:
            decoded = decode_token(request.json.get("token"), False)
            if decoded:
                logger.info("Verified token for user: %s", decoded.get("user_guid"))
                return ApiResult(decoded, status=200).to_response()
            else:
                logger.warn("Error processing token")
                raise ApiException("Error processing token", status=401)
        except jwt.ExpiredSignatureError:
            logger.warn("Token was expired, need a new token")
            raise ApiException(message="Token has expired, please get a new token", status=401)
        except jwt.DecodeError:
            logger.warn("Token was not valid")
            raise ApiException(message="Token was not valid", status=401)
    else:
        logger.error("Invalid request for token")
        raise ApiException(message="Error with request, not json or could not find token in request", status=401)


@auth_blueprint.route('/loginAsAdmin', methods=['GET'])
def login_as_admin():
    if app.config['DEBUG']:
        g.current_user_guid = User.query.filter_by(admin=True).first().id
        token = create_token()
        resp = redirect(url_for('main.home'))
        resp.set_cookie('Bearer', token, expires=datetime.now() + timedelta(hours=app.config['TOKEN_EXPIRE_MINUTES']))
        return resp
    else:
        return "Error not running in debug mode"


@auth_blueprint.route('/application/token/create', methods=['POST'])
def create_application_token():
    logger.info("Creating application token")
    if request.json and request.json.get("application_name"):
        logger.debug("Valid request for a token for: %s", request.json.get("application_name"))
        application = Application(request.json.get("application_name"), "todo_create_guid")
        db.session.add(application)
        db.session.commit()
        if request.args.get("keyOnly") and request.args.get("keyOnly") == "true":
            logger.info("Created application key for: %s guid: %s but only returning key", application.application_name, application.guid)
            return ApiResult(value=application.application_key).to_response()
        else:
            logger.info("Created application key for: %s guid: %s", application.application_name, application.guid)
            return ApiResult(value=application.to_dict(), status=200).to_response()
    else:
        logger.error("Invalid request for application token")
        raise ApiException(message="Error with request, not json or couldn't find application_name")


@auth_blueprint.route('/application/token/verify', methods=['POST'])
def verify_application_token():
    logger.info("Verifying application token")
    if request.json and request.json.get("application_name") and request.json.get("application_key"):
        application = Application.query.filter_by(application_key=request.json.get("application_key"))\
            .filter_by(application_name=request.json.get("application_name")).one_or_none()
        if application:
            logger.info("Application token verified for: %s guid: %s", application.application_name, application.guid)
            if request.args.get("keyOnly") and request.args.get("keyOnly") == "true":
                logger.debug("Only returning key for verified application")
                return ApiResult(value=application.application_key).to_response()
            else:
                logger.debug("Returning full dict for verified application token")
                return ApiResult(value=application.to_dict(), status=200).to_response()
        else:
            logger.warn("Could not verify token for: %s", request.json.get("application_name"))
            raise ApiException(message="Not authorized", status=401)
    else:
        logger.warn("Invalid request to verify token")
        raise ApiException(message="Error with request, not json or couldn't find application_name or application_key")


@auth_blueprint.errorhandler(ApiException)
def handle_api_exception(error: ApiException):
    return error.to_result().to_response()
