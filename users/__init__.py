import os
from users.models import db
from flask import Flask, request, session
from flask.logging import default_handler
from flask_migrate import Migrate
from config import config
import logging
from users import models

migrate = Migrate()

log_level = logging.DEBUG


def create_app(config_name=None):
    global log_level

    if config_name is None:
        config_name = os.environ.get('LISTENUP_APP_CONFIG', 'development')
        print("Starting up in: " + config_name)
    app = Flask(__name__)
    app.config.from_object(config[config_name])
    app.host = '0.0.0.0'

    if app.config.get("LOG_LEVEL"):
        log_level = app.config.get("LOG_LEVEL")

    # Initialize flask extensions
    db.init_app(app)
    migrate.init_app(app=app, db=db)

    # Register web application routes
    from .routes import main as main_blueprint
    from .auth import auth_blueprint
    app.register_blueprint(main_blueprint)
    app.register_blueprint(auth_blueprint)

    return app
