#!/bin/bash


PS3='Where should I deploy?: '
options=("loan_app.nocix" "loan_app2.nocix" "loan_app.vb")
select hostname in "${options[@]}"
do
    ./archive.sh
    scp loan_app.tgz simpleman19@${hostname}:/home/simpleman19/loan_app
    ssh -t simpleman19@${hostname} "
    echo \" Stopping Gunicorn Service\"
    sudo systemctl stop gunicorn &&
    cd /home/simpleman19/loan_app &&
    tar -xf loan_app.tgz &&
    rm loan_app.tgz &&
    source .env/bin/activate &&
    pip install -r requirements.txt &&
    echo \"Starting Gunicorn Service\"
    sudo systemctl start gunicorn"

    read -p "Reset the database (y/n)? " choice
    case "$choice" in
      y|Y )
      echo "Resetting Database"
      ./database.sh
      ;;
      n|N ) echo "Skipping Database Reset";;
      * ) break;;
    esac

    exit
done
