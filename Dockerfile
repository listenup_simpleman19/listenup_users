FROM listenup/listenup_common:v0.37

RUN apt-get update && apt-get install -y libpq-dev

COPY requirements.txt ${appRoot}
RUN pip install -r requirements.txt

ARG SERVICE_NAME
ARG SERVICE_VERSION
ENV SERVICE_NAME $SERVICE_NAME
ENV SERVICE_VERSION $SERVICE_VERSION

COPY . ${appRoot}

USER nobody

EXPOSE 5000

# Start gunicorn
CMD ["./entrypoint.sh"]
