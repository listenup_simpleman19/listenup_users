#!/usr/bin/env bash

PS3='Where should I setup database?: '
options=("postgres.nocix" "postgres.vb")
select hostname in "${options[@]}"
do
    ./archive.sh
    scp loan_app.tgz simpleman19@${hostname}:/home/simpleman19/loan_app
    ssh -t simpleman19@${hostname} "
    cd /home/simpleman19/loan_app &&
    tar -xf loan_app.tgz &&
    rm loan_app.tgz &&
    source .env/bin/activate &&
    pip install -r requirements.txt &&
    python manage.py createdb -d True"

    read -p "Intialize users (y/n)? " choice
    case "$choice" in
      y|Y )
         ssh -t simpleman19@${hostname} "
         cd /home/simpleman19/loan_app &&
         source .env/bin/activate &&
         python manage.py load_users"
      ;;
      n|N ) echo "no";;
      * ) echo "invalid";;
    esac
    exit
done